import React, { useState } from "react";
import { TodosList } from "./todos.js";
import { v4 as uuid } from "uuid";

function App() {

  const [todos, setTodos] = useState(TodosList)
  const [inputText, setInputText] = useState('')

  const handleAddTodo = (e) => {
    if (e.which === 13) {
      const newId = uuid()
      const newTodo = {
        "userId": 1,
        "id": newId,
        "title": inputText,
        "completed": false
      }
      const newTodos = { ...todos }
      newTodos[newId] = newTodo
      setTodos(newTodos)
      setInputText("")
    }
  }

  const handleToggle = (id) => {
    const newTodos = { ...todos }
    newTodos[id].completed = !newTodos[id].completed
    setTodos(newTodos)
  }

  const handleDelete = (id) => {
    const newTodos = { ...todos }
    delete newTodos[id]
    setTodos(newTodos)
  }

  const wipeOut = () => {
    const newTodos = { ...todos }
    for (const todo in newTodos) {
      if (newTodos[todo].completed) {
        delete newTodos[todo]
      }
    }
    setTodos(newTodos)
  }

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          onChange={(e) => setInputText(e.target.value)}
          onKeyDown={(e) => handleAddTodo(e)}
          value={inputText}
          className="new-todo"
          placeholder="What needs to be done?"
          autoFocus />
      </header>
      <TodoList
        todos={Object.values(todos)}
        handleToggle={handleToggle}
        handleDelete={handleDelete}
      />
      <footer
        className="footer">
        <span
          className="todo-count">
          <strong>{Object.values(todos).filter(todo => !todo.completed).length}</strong> item(s) left
          </span>
        <button
          className="clear-completed"
          onClick={() => wipeOut()}
        >Clear completed</button>
      </footer>
    </section>
  );
}

function TodoItem(props) {
  return (
    <li
      className={props.completed ? "completed" : ""}>
      <div
        className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={props.completed}
          onChange={() => props.handleToggle(props.id)}

        />
        <label>{props.title}</label>
        <button
          className="destroy"
          onClick={() => props.handleDelete(props.id)} />
      </div>
    </li>
  );
}

function TodoList(props) {
  return (
    <section
      className="main">
      <ul
        className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            handleToggle={props.handleToggle}
            handleDelete={props.handleDelete}
            key={todo.id}
          />
        ))}
      </ul>
    </section>
  );
}

export default App;
